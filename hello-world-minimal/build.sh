#!/bin/bash

JAVA_HOME="/run/media/taras/Data/share/jdk1.7.0_51"
ANDROID_HOME="/run/media/taras/Data/android-sdk-linux"
DEV_HOME="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

AAPT_PATH="$ANDROID_HOME/build-tools/19.0.1/aapt"
DX_PATH="$ANDROID_HOME/build-tools/19.0.1/dx"
ANDROID_JAR="$ANDROID_HOME/platforms/android-19/android.jar"

PACKAGE_PATH="com/example/helloworldapp"
PACKAGE="com.example.helloworldapp"
MAIN_CLASS="MainActivity"

# R.java precompilation
echo -n "Precompiling R.java...  "
$AAPT_PATH package -f -m -S $DEV_HOME/res -J $DEV_HOME/src -M $DEV_HOME/AndroidManifest.xml -I $ANDROID_JAR || exit 1
echo "Success"

# compilation of the project itself
echo -n "Bare Java project compilation...  "
$JAVA_HOME/bin/javac -d $DEV_HOME/obj -cp $ANDROID_JAR -sourcepath $DEV_HOME/src $DEV_HOME/src/$PACKAGE_PATH/*.java || exit 1
echo "Success"

# recompilation for android java VM
echo -n "Recompilation to DX format...  "
$DX_PATH --dex --output=$DEV_HOME/bin/classes.dex $DEV_HOME/obj || exit 1
echo "Success"

# packing the unsigned .apk
echo -n "Packing to the unsigned apk file...  "
$AAPT_PATH package -f -M $DEV_HOME/AndroidManifest.xml -S $DEV_HOME/res -I $ANDROID_JAR -F $DEV_HOME/bin/AndroidTest.unsigned.apk $DEV_HOME/bin || exit 1
echo "Success"

# generate key
echo -n "Generating key...  "
$JAVA_HOME/bin/keytool -genkey -validity 10000 -dname "CN=AndroidDebug, O=Android, C=US" -keystore $DEV_HOME/AndroidTest.keystore -storepass android -keypass android -alias androiddebugkey -keyalg RSA -v -keysize 2048 || exit 1
echo "Success"

# signing apk with key
echo -n "Signing the apk with the key...  "
$JAVA_HOME/bin/jarsigner -sigalg SHA1withRSA -digestalg SHA1 -keystore $DEV_HOME/AndroidTest.keystore -storepass android -keypass android -signedjar $DEV_HOME/bin/AndroidTest.signed.apk $DEV_HOME/bin/AndroidTest.unsigned.apk androiddebugkey || exit 1
echo "Success"

echo
echo "Compilation finished"
