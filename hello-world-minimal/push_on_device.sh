#!/bin/bash

ANDROID_HOME="/run/media/taras/Data/android-sdk-linux"
ADB="$ANDROID_HOME/platform-tools/adb"
DEV_HOME="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

PACKAGE="com.example.helloworldapp"
MAIN_CLASS="MainActivity"

$ADB uninstall $PACKAGE
$ADB install $DEV_HOME/bin/AndroidTest.signed.apk
$ADB shell am start $PACKAGE/$PACKAGE.$MAIN_CLASS

