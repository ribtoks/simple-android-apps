#!/bin/bash

echo -n "Clearing existing binaries...  "

rm src/com/example/helloworldapp/R.java
rm *.keystore
rm -rf bin/*
rm -r obj/*

JAVA_HOME="/run/media/taras/Data/share/jdk1.7.0_51"
$JAVA_HOME/bin/keytool -delete -alias androiddebugkey

echo "Success"
