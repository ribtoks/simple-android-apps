package ribtoks.bezengi_routes;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

public class RouteAdapter extends BaseAdapter implements Filterable {
	Context ctx;
	LayoutInflater layoutInflater;
	ArrayList<BezengiRoute> routes;
	ArrayList<BezengiRoute> originalRoutesList;

	public RouteAdapter(Context context, ArrayList<BezengiRoute> _routes) {
		ctx = context;
		routes = _routes;
		originalRoutesList = _routes;
		layoutInflater = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return routes.size();
	}

	@Override
	public Object getItem(int arg0) {
		return routes.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	@Override
	public View getView(int index, View convertView, ViewGroup parent) {
		View view = convertView;
	    if (view == null) {
	      view = layoutInflater.inflate(R.layout.route_item, parent, false);
	    }
	    
	    BezengiRoute route = (BezengiRoute)getItem(index);
	    ((TextView)view.findViewById(R.id.routeName)).setText(route.name);
	    ((TextView)view.findViewById(R.id.campName)).setText(route.camp_name);
	    ((TextView)view.findViewById(R.id.mountainName)).setText(route.mountain);
	    ((TextView)view.findViewById(R.id.mountainHeight)).setText(route.mountain_height);
	    ((TextView)view.findViewById(R.id.routeDifficulty)).setText(route.difficulty);
	    
	    if (index % 2 == 0)
	    	view.setBackgroundColor(view.getResources().getColor(R.color.white));
	    else
	    	view.setBackgroundColor(view.getResources().getColor(R.color.gray));
	    
		return view;
	}
	
	protected ArrayList<BezengiRoute> getFilteredResults(CharSequence constraint){
		ArrayList<BezengiRoute> filtered = new ArrayList<BezengiRoute>();
		if (constraint == null || constraint.length() == 0)	{
			filtered = originalRoutesList;
		}
		else {
			constraint = constraint.toString().toLowerCase();
			for (int i = 0; i < originalRoutesList.size(); ++i)	{
				if (originalRoutesList.get(i).matchesConstraint(constraint))
					filtered.add(originalRoutesList.get(i));
			}
		}
		
		return filtered;
	}
	
	@Override
	public Filter getFilter(){
		return new Filter() {
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                routes = (ArrayList<BezengiRoute>) results.values;
                RouteAdapter.this.notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                ArrayList<BezengiRoute> filteredResults = getFilteredResults(constraint);

                FilterResults results = new FilterResults();
                results.values = filteredResults;

                return results;
            }
        };
	}

}
