package ribtoks.bezengi_routes;

public class BezengiRoute {
	String name = "";
	String mountain = "";
	String mountain_height = "";
	String camp_name = "";
	String difficulty = "";
	
	private String name_lowercase;
	private String mountain_lowercase;
	private String camp_name_lowercase;
	private String difficulty_lowercase;
	
	public BezengiRoute(String rawRow){
		String[] parts = rawRow.split(",");
		
		if (parts.length > 0)
			mountain = parts[0];
		
		if (parts.length > 1)
			mountain_height = parts[1];
		
		if (parts.length > 2)
			difficulty = parts[2];
		
		if (parts.length > 3)
			name = parts[3];
		
		if (parts.length > 4)
			camp_name = parts[4];
		
		this.setLowercase();
	}
	
	private void setLowercase(){
		name_lowercase = name.toLowerCase();
		mountain_lowercase = mountain.toLowerCase();
		camp_name_lowercase = camp_name.toLowerCase();
		difficulty_lowercase = difficulty.toLowerCase();
	}
	
	public Boolean matchesConstraint(CharSequence constraint){
		return name_lowercase.contains(constraint) 
				|| mountain_lowercase.contains(constraint)
				|| mountain_height.contains(constraint)
				|| camp_name_lowercase.contains(constraint)
				|| difficulty_lowercase.contains(constraint);
	}
}
