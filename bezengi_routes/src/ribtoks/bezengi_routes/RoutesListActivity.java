package ribtoks.bezengi_routes;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import android.os.Bundle;
import android.app.Activity;
import android.app.ListActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.widget.ArrayAdapter;
import android.widget.EditText;

public class RoutesListActivity extends ListActivity {
	private EditText filterText = null;
	RouteAdapter adapter = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_routes_list);
		
		filterText = (EditText)findViewById(R.id.filterText);
		filterText.addTextChangedListener(filterTextWatcher);
		
		adapter = new RouteAdapter(this, readRoutesFromList());
		
		setListAdapter(adapter);
	}
	
	private ArrayList<BezengiRoute> readRoutesFromList(){
		ArrayList<BezengiRoute> routes = new ArrayList<BezengiRoute>();
		InputStream is = null;
		try
		{
			is = this.getAssets().open("bezengi_routes.csv");
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			String line;
			while ((line = br.readLine()) != null){
				routes.add(new BezengiRoute(line));
			}
		}
		catch(IOException ex){
			
		}
		finally
		{
			try {
					is.close();
			}
			catch(IOException ioex){}
			
		}
		return routes;
	}
	

	private TextWatcher filterTextWatcher = new TextWatcher() {
	
	    public void afterTextChanged(Editable s) {
	    }
	
	    public void beforeTextChanged(CharSequence s, int start, int count,
	            int after) {
	    }
	
	    public void onTextChanged(CharSequence s, int start, int before,
	            int count) {
	        adapter.getFilter().filter(s);
	    }
	
	};
	
	@Override
	protected void onDestroy() {
	    super.onDestroy();
	    filterText.removeTextChangedListener(filterTextWatcher);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_routes_list, menu);
		return true;
	}

}
